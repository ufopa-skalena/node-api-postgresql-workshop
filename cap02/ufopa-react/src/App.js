
import React from 'react';

import './App.css'

import "primereact/resources/themes/lara-light-indigo/theme.css";  //theme
import "primereact/resources/primereact.min.css";                  //core css
import "primeicons/primeicons.css";                                //icons


import UfopaPanel from './ufopa/UfopaPanel'

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';



function App() {

  const leftContents = (
    <React.Fragment>
        <Button label="New" icon="pi pi-plus" className="mr-2" />
        <Button label="Upload" icon="pi pi-upload" className="p-button-success" />
        <i className="pi pi-bars p-toolbar-separator mr-2" />
    </React.Fragment>
);

const rightContents = (
    <React.Fragment>
        <Button icon="pi pi-search" className="mr-2" />
        <Button icon="pi pi-calendar" className="p-button-success mr-2" />
        <Button icon="pi pi-times" className="p-button-danger" />
    </React.Fragment>
);

  
  return (
    <div className="App">


<Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            News
          </Typography>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
    </Box>

    <UfopaPanel/>


    </div>
  );
}

export default App;
