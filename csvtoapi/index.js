const express = require('express')
const bodyParser = require('body-parser')


const csvPlantio='./components/plantio.csv'
const csvVoos='./components/voos.csv'
const csvSales='./components/sales.csv'

const csv=require('csvtojson')

const app = express()
const port = 3000

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.listen(port, () => {
    console.log(`🗒️  ​➡️​ 💬 CSV to REST Server in ${port}.`)
  }); 

app.get('/', (request, response) => {
    response.json({ info: 'Node.js, Express, and Postgres API' })
  });


app.get('/csv/plantio', async (req, res) => {
  csv()
  .fromFile(csvPlantio)
  .then((jsonObj)=>{
      res.send(jsonObj)
  })
})

app.get('/csv/sales', async (req, res) => {
  csv()
  .fromFile(csvSales)
  .then((jsonObj)=>{
      res.send(jsonObj)
  })
})

app.get('/csv/voos', async (req, res) => {
  csv()
  .fromFile(csvVoos)
  .then((jsonObj)=>{
      res.send(jsonObj)
  })
})
